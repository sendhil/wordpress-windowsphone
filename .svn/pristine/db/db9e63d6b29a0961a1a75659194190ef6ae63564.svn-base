﻿<phone:PhoneApplicationPage
    x:Class="WordPress.BlogSettingsPage"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:phone="clr-namespace:Microsoft.Phone.Controls;assembly=Microsoft.Phone"
    xmlns:shell="clr-namespace:Microsoft.Phone.Shell;assembly=Microsoft.Phone"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
	xmlns:converters="clr-namespace:WordPress.Converters"
    SupportedOrientations="Portrait" Orientation="Portrait"
    mc:Ignorable="d" d:DesignHeight="768" d:DesignWidth="480"
    shell:SystemTray.IsVisible="True">

    <phone:PhoneApplicationPage.Resources>
        <converters:BooleanInversionConverter x:Key="BooleanInversionConverter"/>
        <converters:ThumbnailSizeToStringConverter x:Key="ThumbnailSizeToStringConverter"/>
        
        <Style x:Key="PhoneButtonBase" TargetType="ButtonBase">
        	<Setter Property="Background" Value="Transparent"/>
        	<Setter Property="BorderBrush" Value="{StaticResource PhoneForegroundBrush}"/>
        	<Setter Property="Foreground" Value="{StaticResource PhoneForegroundBrush}"/>
        	<Setter Property="BorderThickness" Value="{StaticResource PhoneBorderThickness}"/>
        	<Setter Property="FontFamily" Value="{StaticResource PhoneFontFamilySemiBold}"/>
        	<Setter Property="FontSize" Value="{StaticResource PhoneFontSizeMediumLarge}"/>
        	<Setter Property="Padding" Value="10,3,10,5"/>
        	<Setter Property="Template">
        		<Setter.Value>
        			<ControlTemplate TargetType="ButtonBase">
        				<Grid Background="Transparent">
        					<VisualStateManager.VisualStateGroups>
        						<VisualStateGroup x:Name="CommonStates">
        							<VisualState x:Name="Normal"/>
        							<VisualState x:Name="MouseOver"/>
        							<VisualState x:Name="Pressed">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentContainer">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneBackgroundBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="ButtonBackground">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneForegroundBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="BorderBrush" Storyboard.TargetName="ButtonBackground">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneForegroundBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        							<VisualState x:Name="Disabled">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentContainer">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneDisabledBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="BorderBrush" Storyboard.TargetName="ButtonBackground">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneDisabledBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="ButtonBackground">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="Transparent"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        						</VisualStateGroup>
        					</VisualStateManager.VisualStateGroups>
        					<Border x:Name="ButtonBackground" BorderBrush="{TemplateBinding BorderBrush}" BorderThickness="{TemplateBinding BorderThickness}" Background="{TemplateBinding Background}" CornerRadius="0" Margin="{StaticResource PhoneTouchTargetOverhang}">
        						<ContentControl x:Name="ContentContainer" ContentTemplate="{TemplateBinding ContentTemplate}" Content="{TemplateBinding Content}" Foreground="{TemplateBinding Foreground}" HorizontalContentAlignment="{TemplateBinding HorizontalContentAlignment}" Padding="{TemplateBinding Padding}" VerticalContentAlignment="{TemplateBinding VerticalContentAlignment}"/>
        					</Border>
        				</Grid>
        			</ControlTemplate>
        		</Setter.Value>
        	</Setter>
        </Style>
        <Style x:Key="PhoneRadioButtonCheckBoxBase" BasedOn="{StaticResource PhoneButtonBase}" TargetType="ToggleButton">
        	<Setter Property="Background" Value="{StaticResource PhoneRadioCheckBoxBrush}"/>
        	<Setter Property="BorderBrush" Value="{StaticResource PhoneRadioCheckBoxBrush}"/>
        	<Setter Property="FontSize" Value="{StaticResource PhoneFontSizeMedium}"/>
        	<Setter Property="FontFamily" Value="{StaticResource PhoneFontFamilyNormal}"/>
        	<Setter Property="HorizontalContentAlignment" Value="Left"/>
        	<Setter Property="VerticalContentAlignment" Value="Center"/>
        	<Setter Property="Padding" Value="0"/>
        </Style>
        <Style x:Key="RightAlignedCheckbox" BasedOn="{StaticResource PhoneRadioButtonCheckBoxBase}" TargetType="CheckBox">
        	<Setter Property="Template">
        		<Setter.Value>
        			<ControlTemplate TargetType="CheckBox">
        				<Grid Background="Transparent">
        					<VisualStateManager.VisualStateGroups>
        						<VisualStateGroup x:Name="CommonStates">
        							<VisualState x:Name="Normal"/>
        							<VisualState x:Name="MouseOver"/>
        							<VisualState x:Name="Pressed">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="CheckBackground">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneRadioCheckBoxPressedBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="BorderBrush" Storyboard.TargetName="CheckBackground">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneRadioCheckBoxPressedBorderBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Fill" Storyboard.TargetName="CheckMark">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneRadioCheckBoxCheckBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Fill" Storyboard.TargetName="IndeterminateMark">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneRadioCheckBoxCheckBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        							<VisualState x:Name="Disabled">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="CheckBackground">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneRadioCheckBoxDisabledBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="BorderBrush" Storyboard.TargetName="CheckBackground">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneDisabledBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Fill" Storyboard.TargetName="CheckMark">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneRadioCheckBoxCheckDisabledBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Fill" Storyboard.TargetName="IndeterminateMark">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneRadioCheckBoxCheckDisabledBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentContainer">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PhoneDisabledBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        						</VisualStateGroup>
        						<VisualStateGroup x:Name="CheckStates">
        							<VisualState x:Name="Checked">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Visibility" Storyboard.TargetName="CheckMark">
        										<DiscreteObjectKeyFrame KeyTime="0">
        											<DiscreteObjectKeyFrame.Value>
        												<Visibility>Visible</Visibility>
        											</DiscreteObjectKeyFrame.Value>
        										</DiscreteObjectKeyFrame>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        							<VisualState x:Name="Unchecked"/>
        							<VisualState x:Name="Indeterminate">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Visibility" Storyboard.TargetName="IndeterminateMark">
        										<DiscreteObjectKeyFrame KeyTime="0">
        											<DiscreteObjectKeyFrame.Value>
        												<Visibility>Visible</Visibility>
        											</DiscreteObjectKeyFrame.Value>
        										</DiscreteObjectKeyFrame>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        						</VisualStateGroup>
        					</VisualStateManager.VisualStateGroups>
        					<Grid>
        						<Grid.ColumnDefinitions>        							
        							<ColumnDefinition Width="*"/>
									<ColumnDefinition Width="32"/>
        						</Grid.ColumnDefinitions>
        						<ContentControl x:Name="ContentContainer" ContentTemplate="{TemplateBinding ContentTemplate}" Content="{TemplateBinding Content}" Grid.Column="0" Foreground="{TemplateBinding Foreground}" HorizontalContentAlignment="{TemplateBinding HorizontalContentAlignment}" Padding="{TemplateBinding Padding}" VerticalContentAlignment="{TemplateBinding VerticalContentAlignment}"/>
								<Border x:Name="CheckBackground" Grid.Column="1" BorderBrush="{TemplateBinding Background}" BorderThickness="{StaticResource PhoneBorderThickness}" Background="{TemplateBinding Background}" HorizontalAlignment="Left" Height="32" IsHitTestVisible="False" VerticalAlignment="Center" Width="32"/>
        						<Rectangle x:Name="IndeterminateMark" Grid.Column="1" Fill="{StaticResource PhoneRadioCheckBoxCheckBrush}" HorizontalAlignment="Center" Height="16" IsHitTestVisible="False" Grid.Row="0" Visibility="Collapsed" VerticalAlignment="Center" Width="16"/>
        						<Path x:Name="CheckMark" Grid.Column="1" Data="M0,119 L31,92 L119,185 L267,0 L300,24 L122,250 z" Fill="{StaticResource PhoneRadioCheckBoxCheckBrush}" HorizontalAlignment="Center" Height="18" IsHitTestVisible="False" Stretch="Fill" StrokeThickness="2" StrokeLineJoin="Round" Visibility="Collapsed" VerticalAlignment="Center" Width="24"/>
        						
        					</Grid>
        				</Grid>
        			</ControlTemplate>
        		</Setter.Value>
        	</Setter>
        </Style>

    </phone:PhoneApplicationPage.Resources>

    <!--LayoutRoot is the root grid where all page content is placed-->
    <Grid x:Name="LayoutRoot" Style="{StaticResource LayoutRootStyle}">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*"/>
        </Grid.RowDefinitions>

        <!--TitlePanel contains the name of the application and page title-->
        <StackPanel x:Name="TitlePanel" Grid.Row="0" Margin="12,17,0,28">
            <Grid Margin="0,0,8,0">
                <Grid.RowDefinitions>
                    <RowDefinition/>
                </Grid.RowDefinitions>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto"/>
                    <ColumnDefinition Width="Auto"/>
                </Grid.ColumnDefinitions>
                <Image Source="Images/wp-logo.png" Stretch="None" Width="227" Height="50" Margin="0" HorizontalAlignment="Left" RenderTransformOrigin="0.52,-0.18" d:LayoutOverrides="Height"/>
            </Grid>
            <TextBlock x:Name="PageTitle" Text="{Binding PageTitles.Settings, Source={StaticResource StringTable}}" Margin="9,-7,8,0" Style="{StaticResource PhoneTextTitle1Style}">
                <TextBlock.Foreground>
            		<SolidColorBrush Color="{StaticResource WordPressGrey}"/>
            	</TextBlock.Foreground>
            </TextBlock>
        </StackPanel>
        <ScrollViewer x:Name="ContentPanel" Margin="12,0" Grid.Row="1" >
            <StackPanel Orientation="Vertical" Width="456">
                <Border Background="{StaticResource SectionHeaderBackgroundBrush}">
                    <TextBlock TextWrapping="Wrap" Text="{Binding ControlsText.AccountDetails, Source={StaticResource StringTable}}" 
								FontSize="{StaticResource SectionHeaderFontSize}"
								Foreground="{StaticResource WordPressBlueBrush}" />
                </Border>
                <TextBlock TextWrapping="Wrap" FontSize="{StaticResource ControlFontSize}" 
							Text="{Binding ControlsText.Username, Source={StaticResource StringTable}}" 
							Foreground="{StaticResource WordPressGreyBrush}"/>
                <TextBox x:Name="usernameTextBox" 
                         Style="{StaticResource TextBoxStyle}" 
                         Template="{StaticResource FocusedTextBoxTemplate}"
                         TextWrapping="Wrap" 
                         Text="{Binding Username, Mode=TwoWay}" 
                         FontSize="{StaticResource ControlFontSize}"                          
                         />
                <TextBlock TextWrapping="Wrap" FontSize="{StaticResource ControlFontSize}" 
						Text="{Binding ControlsText.Password, Source={StaticResource StringTable}}"
						Foreground="{StaticResource WordPressGreyBrush}"/>
                <PasswordBox x:Name="passwordTextBox" 
                             FontSize="{StaticResource ControlFontSize}" 
                             Style="{StaticResource PasswordBoxStyle}" 
                             Template="{StaticResource FocusedPasswordBoxTemplate}"
                             Password="{Binding Password, Mode=TwoWay}"
                             />
                <TextBlock TextWrapping="Wrap" Text="{Binding ControlsText.Apikey, Source={StaticResource StringTable}}"
							FontSize="{StaticResource ControlFontSize}" 
        					Foreground="{StaticResource WordPressGreyBrush}"/>
                <PasswordBox x:Name="apikeyTextBox" 
                             FontSize="{StaticResource ControlFontSize}" 
                             Style="{StaticResource PasswordBoxStyle}" 
                             Template="{StaticResource FocusedPasswordBoxTemplate}"
                             Password="{Binding ApiKey, Mode=TwoWay}"
                            />
                <Border Background="{StaticResource SectionHeaderBackgroundBrush}">
                    <TextBlock Text="{Binding ControlsText.Media, Source={StaticResource StringTable}}" 
								Foreground="{StaticResource WordPressBlueBrush}"
								FontSize="{StaticResource SectionHeaderFontSize}"/>
                </Border>
                <TextBlock Text="{Binding ControlsText.PlaceImage, Source={StaticResource StringTable}}"
								Foreground="{StaticResource WordPressGreyBrush}"
								FontSize="{StaticResource ControlFontSize}"/>
                <StackPanel Orientation="Horizontal">
                    <RadioButton x:Name="aboveTextRadioButton" 
                                 Content="{Binding ControlsText.AboveText, Source={StaticResource StringTable}}" 
                                 IsChecked="{Binding PlaceImageAboveText, Mode=TwoWay}" 
                                 GroupName="imagePlacement" 
                                 Margin="0,0,0,5" 
                                 FontSize="{StaticResource ControlFontSize}"
								 Foreground="{StaticResource WordPressGreyBrush}"
								 Background="{StaticResource SectionHeaderBackgroundBrush}"/>
                    <RadioButton x:Name="belowTextRadioButton" 
                                 Content="{Binding ControlsText.BelowText, Source={StaticResource StringTable}}" 
                                 GroupName="imagePlacement" 
                                 Margin="0,0,0,5" 
                                 FontSize="{StaticResource ControlFontSize}"
                                 IsChecked="{Binding PlaceImageAboveText, Mode=TwoWay, Converter={StaticResource BooleanInversionConverter}}"                               
								 Foreground="{StaticResource WordPressGreyBrush}"
								 Background="{StaticResource SectionHeaderBackgroundBrush}"/>					
                </StackPanel>
                <Grid>
                	<Grid.ColumnDefinitions>
                		<ColumnDefinition/>
                		<ColumnDefinition Width="Auto"/>
                	</Grid.ColumnDefinitions>
                	<TextBlock TextWrapping="Wrap" 
                		    Text="{Binding ControlsText.MaximumThumbnailPixelWidth, Source={StaticResource StringTable}}"
                		    FontSize="{StaticResource ControlFontSize}"
                		    Foreground="{StaticResource WordPressGreyBrush}" 
                            Margin="0" 
                            VerticalAlignment="Center" />
                	<Button x:Name="thumbnailPixelWidthButton"
                		    FontSize="{StaticResource ControlFontSize}"
                		    Content="{Binding ThumbnailPixelWidth, Converter={StaticResource ThumbnailSizeToStringConverter}}"
                		    Style="{StaticResource BasicButtonStyle}"
                		    Click="OnThumbnailPixelWidthButtonClick" 
                            Margin="0,0,-10,0" 
                            HorizontalAlignment="Right" 
                            Grid.Column="2" 
                            MinWidth="100" 
                            MinHeight="36" 
                            VerticalAlignment="Center" 
                            Padding="10,0" />
                </Grid>
				<CheckBox x:Name="alignThumbnailCheckBox" 
                          Content="{Binding ControlsText.AlignThumbnailToCenter, Source={StaticResource StringTable}}" 
                          FontSize="{StaticResource ControlFontSize}"
                          IsChecked="{Binding AlignThumbnailToCenter, Mode=TwoWay}"
                          Foreground="{StaticResource WordPressGreyBrush}" 
                          Style="{StaticResource RightAlignedCheckbox}"
                          Background="{StaticResource SectionHeaderBackgroundBrush}" Margin="0,10"/>                
                <CheckBox x:Name="uploadCheckBox" 
                          Content="{Binding ControlsText.UploadAndLinkToFullImage, Source={StaticResource StringTable}}" 
                          FontSize="{StaticResource ControlFontSize}"
                          IsChecked="{Binding CreateLinkToFullImage, Mode=TwoWay}"
                          Foreground="{StaticResource WordPressGreyBrush}"
                          Style="{StaticResource RightAlignedCheckbox}"
                          Background="{StaticResource SectionHeaderBackgroundBrush}" Margin="0,0,0,20"/>
            </StackPanel>
        </ScrollViewer>
    </Grid>
</phone:PhoneApplicationPage>
