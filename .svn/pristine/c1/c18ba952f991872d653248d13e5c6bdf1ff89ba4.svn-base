﻿<phone:PhoneApplicationPage 
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" 
    xmlns:phone="clr-namespace:Microsoft.Phone.Controls;assembly=Microsoft.Phone" 
    xmlns:shell="clr-namespace:Microsoft.Phone.Shell;assembly=Microsoft.Phone" 
    xmlns:controls="clr-namespace:Microsoft.Phone.Controls;assembly=Microsoft.Phone.Controls" 
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
    xmlns:local="clr-namespace:WordPress" 
    xmlns:converters="clr-namespace:WordPress.Converters" 
    xmlns:controlsPrimitives="clr-namespace:Microsoft.Phone.Controls.Primitives;assembly=Microsoft.Phone.Controls" 
    x:Class="WordPress.ModerateCommentsPage"
    SupportedOrientations="Portrait" mc:Ignorable="d" Orientation="Portrait"
    d:DesignHeight="768" d:DesignWidth="480"
    shell:SystemTray.IsVisible="True">
    <phone:PhoneApplicationPage.Resources>

        <local:MasterViewModel x:Key="MasterViewModelDataSource" d:IsDataSource="True"/>

        <converters:CommentStatusGroupingConverter x:Key="ApprovedCommentConverter" Status="approve"/>
        <converters:CommentStatusGroupingConverter x:Key="UnapprovedCommentConverter" Status="hold"/>
        <converters:CommentStatusGroupingConverter x:Key="SpamCommentConverter" Status="spam"/>

    </phone:PhoneApplicationPage.Resources>

    <phone:PhoneApplicationPage.FontFamily>
        <StaticResource ResourceKey="PhoneFontFamilyNormal"/>
    </phone:PhoneApplicationPage.FontFamily>
    <phone:PhoneApplicationPage.FontSize>
        <StaticResource ResourceKey="PhoneFontSizeNormal"/>
    </phone:PhoneApplicationPage.FontSize>
    <phone:PhoneApplicationPage.Foreground>
        <StaticResource ResourceKey="PhoneForegroundBrush"/>
    </phone:PhoneApplicationPage.Foreground>

    <!--LayoutRoot is the root grid where all page content is placed-->
    <Grid x:Name="LayoutRoot" Style="{StaticResource LayoutRootStyle}" DataContext="{Binding Source={StaticResource MasterViewModelDataSource}}">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*"/>
        </Grid.RowDefinitions>

        <!--TitlePanel contains the name of the application and page title-->
        <StackPanel x:Name="TitlePanel" Grid.Row="0" Margin="12,17,0,28">
            <Grid Margin="0,0,8,0">
                <Grid.RowDefinitions>
                    <RowDefinition/>
                </Grid.RowDefinitions>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto"/>
                    <ColumnDefinition Width="Auto"/>
                </Grid.ColumnDefinitions>
                <Image Source="Images/wp-logo.png" Stretch="None" Width="227" Height="50" Margin="0" HorizontalAlignment="Left" RenderTransformOrigin="0.52,-0.18" d:LayoutOverrides="Height"/>
            </Grid>
            <TextBlock x:Name="PageTitle" Text="{Binding PageTitles.ModerateComments, Source={StaticResource StringTable}}" Margin="9,-7,8,0" Style="{StaticResource PhoneTextTitle1Style}">
                <TextBlock.Foreground>
            		<SolidColorBrush Color="{StaticResource WordPressGrey}"/>
            	</TextBlock.Foreground>
            </TextBlock>
        </StackPanel>

        <!--ContentPanel - place additional content here-->
        <Grid x:Name="ContentPanel" Grid.Row="1" Margin="12,0,12,0">
            <controls:Pivot x:Name="commentsPivot" SelectionChanged="OnCommentsPivotSelectionChanged" >
                <controls:Pivot.Foreground>
                    <SolidColorBrush Color="{StaticResource WordPressBlue}"/>
                </controls:Pivot.Foreground>
                <controls:PivotItem Header="{Binding PageTitles.All, Source={StaticResource StringTable}}">
                    <ListBox x:Name="allCommentsListBox" ItemsSource="{Binding CurrentBlog.Comments}" SelectionMode="Multiple" 
                             ItemContainerStyle="{StaticResource CommentListItemStyle}"/>
                </controls:PivotItem>
                <controls:PivotItem Header="{Binding PageTitles.Approve, Source={StaticResource StringTable}}">
                    <ListBox x:Name="approvedCommentsListBox" ItemsSource="{Binding CurrentBlog.Comments, Converter={StaticResource ApprovedCommentConverter}}" 
                             SelectionMode="Multiple" ItemContainerStyle="{StaticResource CommentListItemStyle}"/>
                </controls:PivotItem>
                <controls:PivotItem Header="{Binding PageTitles.Unapprove, Source={StaticResource StringTable}}">
                    <ListBox x:Name="unapprovedCommentsListBox" ItemsSource="{Binding CurrentBlog.Comments, Converter={StaticResource UnapprovedCommentConverter}}" 
                             SelectionMode="Multiple" ItemContainerStyle="{StaticResource CommentListItemStyle}"/>
                </controls:PivotItem>
                <controls:PivotItem Header="{Binding PageTitles.Spam, Source={StaticResource StringTable}}">
                    <ListBox x:Name="spamCommentsListBox" ItemsSource="{Binding CurrentBlog.Comments, Converter={StaticResource SpamCommentConverter}}" 
                             SelectionMode="Multiple" ItemContainerStyle="{StaticResource CommentListItemStyle}"/>
                </controls:PivotItem>
            </controls:Pivot>
        </Grid>
    </Grid>


</phone:PhoneApplicationPage>
