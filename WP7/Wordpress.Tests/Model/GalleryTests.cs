﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Microsoft.Silverlight.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordPress.Model;

namespace Wordpress.Tests.Models
{
    [TestClass]
    public class GalleryTests : SilverlightTest
    {
        private List<string> _mediaIds;

        [TestInitialize]
        public void Setup()
        {
            _mediaIds = new List<string>() { "1", "2", "3" };
        }
        [TestMethod]
        public void ShouldReturnBlankStringWithNoMediaIds()
        {
            Gallery gallery = new Gallery();
            List<string> mediaIds = new List<string>();
            Assert.AreEqual(String.Empty, gallery.GenerateShortcode(mediaIds));
        }

        [TestMethod]
        public void ShouldIncludeMediaIdsIfIncluded()
        {
            var gallery = new Gallery();
            var result = gallery.GenerateShortcode(_mediaIds);
            var searchString = String.Format("ids=\"{0}\"", String.Join(",", _mediaIds));
            StringAssert.Contains(result, searchString);
        }

        [TestMethod]
        public void ShouldIncludeColumnsAttributeIfColumnsDoesNotEqualDefault()
        {
            var gallery = new Gallery();
            gallery.Columns = 2;
            var result = gallery.GenerateShortcode(_mediaIds);
            var searchString = "columns=\"2\"";
            StringAssert.Contains(result, searchString);
        }

        [TestMethod]
        public void ShouldNotIncludeOrderByAttributeIfRandomOrderIsFalse()
        {
            var gallery = new Gallery();
            gallery.RandomOrder = false;
            var result = gallery.GenerateShortcode(_mediaIds);
            var searchString = "orderby=\"rand\"";
            StringAssert.DoesNotMatch(result, new Regex(searchString));
        }

        [TestMethod]
        public void ShouldIncludeOrderByAttributeIfRandomOrderIsTrue()
        {
            var gallery = new Gallery();
            gallery.RandomOrder = true;
            var result = gallery.GenerateShortcode(_mediaIds);
            var searchString = "orderby=\"rand\"";
            StringAssert.Contains(result, searchString);
        }

        [TestMethod]
        public void ShouldNotIncludeLinkAttributeIfLinkToIsSetToAttachment()
        {
            var gallery = new Gallery();
            gallery.LinkTo = eGalleryLinkTo.AttachmentPage;
            var result = gallery.GenerateShortcode(_mediaIds);
            var searchString = "link";
            StringAssert.DoesNotMatch(result, new Regex(searchString));
        }

        [TestMethod]
        public void ShouldIncludeLinkAttributeIfLinkToIsSetToMediaFile()
        {
            var gallery = new Gallery();
            gallery.LinkTo = eGalleryLinkTo.MediaFile;
            var result = gallery.GenerateShortcode(_mediaIds);
            var searchString = "link=\"file\"";
            StringAssert.Contains(result, searchString);
        }

        [TestMethod]
        public void ShouldNotIncludeTypeAttributeIfTypeIsSetToDefault()
        {
            var gallery = new Gallery();
            gallery.Type = eGalleryType.Default;
            var result = gallery.GenerateShortcode(_mediaIds);
            var searchString = "type";
            StringAssert.DoesNotMatch(result, new Regex(searchString));
        }

        [TestMethod]
        public void ShouldIncludeCorrectTypeWhenTypeIsSetToTiles()
        {
            var gallery = new Gallery();
            gallery.Type = eGalleryType.Tiles;
            var result = gallery.GenerateShortcode(_mediaIds);
            var searchString = "type=\"rectangular\"";
            StringAssert.Contains(result, searchString);
        }

        [TestMethod]
        public void ShouldIncludeCorrectTypeWhenTypeIsSetToSquareTiles()
        {
            var gallery = new Gallery();
            gallery.Type = eGalleryType.SquareTiles;
            var result = gallery.GenerateShortcode(_mediaIds);
            var searchString = "type=\"square\"";
            StringAssert.Contains(result, searchString);
        }

        [TestMethod]
        public void ShouldIncludeCorrectTypeWhenTypeIsSetToCircle()
        {
            var gallery = new Gallery();
            gallery.Type = eGalleryType.Circles;
            var result = gallery.GenerateShortcode(_mediaIds);
            var searchString = "type=\"circle\"";
            StringAssert.Contains(result, searchString);
        }

        [TestMethod]
        public void ShouldIncludeCorrectTypeWhenTypeIsSetToSlideshow()
        {
            var gallery = new Gallery();
            gallery.Type = eGalleryType.Slideshow;
            var result = gallery.GenerateShortcode(_mediaIds);
            var searchString = "type=\"slideshow\"";
            StringAssert.Contains(result, searchString);
        }

        [TestMethod]
        public void ShouldIncludeCombinedAttributes()
        {
            var gallery = new Gallery();
            gallery.RandomOrder = true;
            gallery.Columns = 2;
            gallery.LinkTo = eGalleryLinkTo.MediaFile;
            gallery.Type = eGalleryType.Slideshow;
            var result = gallery.GenerateShortcode(_mediaIds);

            StringAssert.Contains(result, "orderby=\"rand\"");
            StringAssert.Contains(result, "type=\"slideshow\"");
            StringAssert.Contains(result, "link=\"file\"");
            StringAssert.Contains(result, "columns=\"2\"");
            StringAssert.Contains(result, String.Format("ids=\"{0}\"", String.Join(",", _mediaIds)));
        }
    }
}
