﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using Microsoft.Silverlight.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordPress.Model;

namespace Wordpress.Tests.Models
{
    [TestClass]
    public class PostTests : SilverlightTest
    {
        private string _mediaUrl;
        private string _postDescription;
        
        [TestMethod]
        public void IndicatesPostIsLocalDraftIfPostStatusIsLocalDraft()
        {
            var post = new Post();
            post.PostStatus = "localdraft";
            Assert.IsTrue(post.IsLocalDraft());
        }

        [TestMethod]
        public void IndicatesPostIsNotLocalDraftIfPostStatusIsNotLocalDraft()
        {
            var post = new Post();
            post.PostStatus = "publish";
            Assert.IsFalse(post.IsLocalDraft());
        }

        [TestMethod]
        public void IndicatesHasMediaIfThereIsAtLeastOneMediaItem()
        {
            var post = new Post();
            post.Media.Add(new Media());
            Assert.IsTrue(post.HasMedia());
        }

        [TestMethod]
        public void IndicatesDoesNotHaveMediaIfThereAreNoMediaItems()
        {
            var post = new Post();
            Assert.IsFalse(post.HasMedia());
        }

        [TestMethod]
        public void IndicatesDoesNotHaveMediaIfMediaIsNull()
        {
            var post = new Post();
            post.Media = null;

            Assert.IsFalse(post.HasMedia());
        }

        [TestMethod]
        public void AddsTagline()
        {
            var post = new Post();
            post.Description = "Post Description";
            post.AddTagline("Tagline");
            StringAssert.Contains(post.Description, "Tagline");
        }

        [TestMethod]
        public void GenerateImageMarkupGeneratesNoMarkupIfThereIsNoMedia()
        {
            var post = new Post();
            post.Description = "Description";
            post.GenerateImageMarkup(true, true);
            Assert.AreEqual(post.Description, "Description");
        }

        private Post GetPost()
        {
            _postDescription = "Post Description";
            return new Post() { Description = _postDescription };
        }

        private Media GetMedia()
        {
            return GetMedia(eMediaPlacement.BlogPreference);
        }

        private Media GetMedia(eMediaPlacement placement)
        {
            _mediaUrl = "http://www.wordpress.com/image.jpg";
            Media media = new Media();
            media.placement = placement;
            media.Url = _mediaUrl;
            media.Id = "1";
            return media;
        }

        [TestMethod]
        public void GenerateImageMarkupGeneratesImageBeforeIfExplicitlySetAndNoGallery()
        {
            var post = GetPost();
            post.Media.Add(GetMedia(eMediaPlacement.Before));
            post.GenerateImageMarkup(true, false);
            int mediaIndex = post.Description.IndexOf(_mediaUrl);
            int descriptionIndex = post.Description.IndexOf(_postDescription);
            Assert.IsTrue(mediaIndex < descriptionIndex);
        }

        [TestMethod]
        public void GenerateImageMarkupGeneratesImageAfterIfExplicitlySetAndNoGallery()
        {
            var post = GetPost();
            post.Media.Add(GetMedia(eMediaPlacement.After));
            post.GenerateImageMarkup(true, false);
            int mediaIndex = post.Description.IndexOf(_mediaUrl);
            int descriptionIndex = post.Description.IndexOf(_postDescription);
            Assert.IsTrue(mediaIndex > descriptionIndex);
        }

        [TestMethod]
        public void GenerateImageMarkupGeneratesImageBeforeIfBlogPreferenceSetAndNoGallery()
        {
            var post = GetPost();
            post.Media.Add(GetMedia(eMediaPlacement.BlogPreference));
            post.GenerateImageMarkup(true, false);
            int mediaIndex = post.Description.IndexOf(_mediaUrl);
            int descriptionIndex = post.Description.IndexOf(_postDescription);
            Assert.IsTrue(mediaIndex < descriptionIndex);
        }

        [TestMethod]
        public void GenerateImageMarkupGeneratesImageAfterIfBlogPreferenceSetAndNoGallery_1()
        {
            // Explicit before and after
            var post = GetPost();
            post.Media.Add(GetMedia(eMediaPlacement.BlogPreference));
            post.GenerateImageMarkup(false, false);
            int mediaIndex = post.Description.IndexOf(_mediaUrl);
            int descriptionIndex = post.Description.IndexOf(_postDescription);
            Assert.IsTrue(mediaIndex > descriptionIndex);
        }

        [TestMethod]
        public void GenerateImageMarkupGeneratesImageBeforeAndAfterIfBasedOnIndividualMediaSettings_2()
        {
            // Explicit after and before
            var post = GetPost();
            post.Media.Add(GetMedia(eMediaPlacement.Before));
            post.Media.Add(GetMedia(eMediaPlacement.After));
            string secondMediaUrl = "http://www.wordpress.org/image.jpg";
            post.Media[1].Url = secondMediaUrl;
            post.GenerateImageMarkup(false, false);
            int mediaBeforeIndex = post.Description.IndexOf(_mediaUrl);
            int mediaAfterIndex = post.Description.IndexOf(secondMediaUrl);
            int descriptionIndex = post.Description.IndexOf(_postDescription);
            Assert.IsTrue(mediaBeforeIndex < descriptionIndex);
            Assert.IsTrue(mediaAfterIndex > descriptionIndex);
        }

        [TestMethod]
        public void GenerateImageMarkupGeneratesImageBeforeAndAfterIfBasedOnIndividualMediaSettings_3()
        {
            // Implicit before, explicit after
            var post = GetPost();
            post.Media.Add(GetMedia(eMediaPlacement.BlogPreference));
            post.Media.Add(GetMedia(eMediaPlacement.After));
            string secondMediaUrl = "http://www.wordpress.org/image.jpg";
            post.Media[1].Url = secondMediaUrl;
            post.GenerateImageMarkup(true, false);
            int mediaBeforeIndex = post.Description.IndexOf(_mediaUrl);
            int mediaAfterIndex = post.Description.IndexOf(secondMediaUrl);
            int descriptionIndex = post.Description.IndexOf(_postDescription);
            Assert.IsTrue(mediaBeforeIndex < descriptionIndex);
            Assert.IsTrue(mediaAfterIndex > descriptionIndex);
        }

        [TestMethod]
        public void GenerateImageMarkupGeneratesImageBeforeAndAfterIfBasedOnIndividualMediaSettings_4()
        {
            // Implicit after, explicit before
            var post = GetPost();
            post.Media.Add(GetMedia(eMediaPlacement.BlogPreference));
            post.Media.Add(GetMedia(eMediaPlacement.Before));
            string secondMediaUrl = "http://www.wordpress.org/image.jpg";
            post.Media[1].Url = secondMediaUrl;
            post.GenerateImageMarkup(false, false);
            int mediaBeforeIndex = post.Description.IndexOf(secondMediaUrl);
            int mediaAfterIndex = post.Description.IndexOf(_mediaUrl);
            int descriptionIndex = post.Description.IndexOf(_postDescription);
            Assert.IsTrue(mediaBeforeIndex < descriptionIndex);
            Assert.IsTrue(mediaAfterIndex > descriptionIndex);
        }

        [TestMethod]
        public void GenerateImageMarkupGeneratesGalleryIdsBefore()
        {
            var post = GetPost();
            post.Gallery.ContentBelow = false;
            post.Media.Add(GetMedia());
            post.GenerateImageMarkup(true, true);

            int descriptionIndex = post.Description.IndexOf(_postDescription);
            int galleryIdsIndex = post.Description.IndexOf("[gallery");

            Assert.IsTrue(galleryIdsIndex < descriptionIndex);
        }

        [TestMethod]
        public void GenerateImageMarkupGeneratesGalleryIdsAfter()
        {
            var post = GetPost();
            post.Gallery.ContentBelow = true;
            post.Media.Add(GetMedia());
            post.GenerateImageMarkup(true, true);

            int descriptionIndex = post.Description.IndexOf(_postDescription);
            int galleryIdsIndex = post.Description.IndexOf("[gallery");

            Assert.IsTrue(galleryIdsIndex > descriptionIndex);
        }

        [TestMethod]
        public void GenerateImageMarkupDoesNotIncludeFeaturedImageInGalleryIds()
        {
            var post = GetPost();
            post.Gallery.ContentBelow = true;
            post.Media.Add(GetMedia());
            post.Media.Add(GetMedia());
            
            string secondMediaId = "featuredMediaId";
            post.Media[1].Id = secondMediaId;
            post.Media[1].IsFeatured = true;
            
            post.GenerateImageMarkup(true, true);

            int galleryIdsIndex = post.Description.IndexOf("[gallery");

            StringAssert.Contains(post.Description, "[gallery");
            StringAssert.DoesNotMatch(post.Description, new Regex(secondMediaId));
        }

        [TestMethod]
        public void GenerateImageMarkupSetsThumbnailIdForFeaturedImage()
        {
            var post = GetPost();
            post.Gallery.ContentBelow = true;
            post.Media.Add(GetMedia());
            post.Media.Add(GetMedia());

            string secondMediaId = "featuredMediaId";
            post.Media[1].Id = secondMediaId;
            post.Media[1].IsFeatured = true;

            post.GenerateImageMarkup(true, true);
            Assert.AreEqual(post.PostThumbnail, secondMediaId);
        }
    }
}
